
Descargar archivos de acceso libre desde springer a partir de lista de paginas de descarga

Instalar dependencias
```
pip3 install -r requerimientos.txt
```

ejecutar script:
```bash
python3 main.py 
```

en el archivo lista_enlaces.txt ignora las lineas que empiezan por una cadena diferente a `http`, por ejemplo:
Sera ignorada las lineas 1 y 3, pero se tiene en cuenta las lineas 2 y 4:
```
-http://link.pagina.com/openurl
http://link.pagina.com/openurl
-http://link.pagina.com/openurl
http://link.pagina.com/openurl
```

El proceso se detiene en la linea vacia, es decir, se ignoran todas las lineas despues de la linea vacia, por ejemplo:
En la linea 3 el programa finaliza

```
-http://link.pagina.com/openurl
http://link.pagina.com/openurl

http://link.pagina.com/openurl
http://link.pagina.com/openurl
```

Puede cambiar el tiempo de espera entre archivos descargados, para evitar que bloqueen las descargas si detectan muchas en poco tiempo
```py
time.sleep(5)
```