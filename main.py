 #!/usr/bin/python

import requests 
from bs4 import BeautifulSoup 
import os
import re
import time
import os.path
from os import path

def read_text():
    # leer lista de paginas de descarga
    lista_ar = open("./lista_enlaces.txt", "r")
    lista_articulos = lista_ar.readlines()
    # print(lista_ar.read())
    # lista_ar.close()

    # lista_articulos = ["https://link.springer.com/book/10.1007%2F978-3-319-92429-8"]
    urlbase = "https://link.springer.com"
    dir_descargas = "descargas"
    linea = 0

    for articulo in lista_articulos:
        linea = linea + 1
        if(articulo == "" or articulo == '\n'):
            break
        if(articulo[:4] != 'http'):
            continue
        my_req = requests.get(articulo)
        my_data = my_req.text 
        my_soup = BeautifulSoup(my_data, 'html.parser') 

        # obtener nombre articulo
        try:
            nombre_articulo = my_soup.find('div', class_='page-title').h1.text 
            # print(nombre_articulo)
            nombre_articulo = nombre_articulo  + '.pdf'
        except:
            print("fallo obtener nombre")
            nombre_articulo = my_soup.find('title').text
            # print(nombre_articulo)
            nombre_articulo = nombre_articulo.split("|")[0] + '.pdf'

        nombre_articulo = nombre_articulo.replace(' ', '_')
        nombre_articulo = nombre_articulo.replace('/', '_')
        nombre_articulo = nombre_articulo.replace(',', '')
        nombre_articulo = nombre_articulo.replace('\\', '_')
        print("[" + str(linea) + "] " + nombre_articulo)

        # obtener categoria o paquete
        try:
            categoria = my_soup.find('a', id='ebook-package').text 
            print("categoria: "+categoria)
        except:
            print("categoria desconocida")
            categoria = "otros"

        categoria = categoria.replace(' ', '_')
        categoria = categoria.replace('/', '_')
        categoria = categoria.replace('\\', '_')

        if(not path.exists(dir_descargas + "/" +categoria )):
            print("Creando directorio de categoria ...")
            os.system("mkdir -p "+ dir_descargas + "/" +categoria )

        if(path.exists(dir_descargas + "/" + nombre_articulo)):
            print("moviendo archivo ...")
            os.system("mv "+ dir_descargas + "/" + nombre_articulo + " " + dir_descargas + "/" +categoria )
            continue

        if(path.exists(dir_descargas + "/" +categoria + "/" + nombre_articulo)):
            print("IGNORADO ...")
            continue
        try:
            # obtener elemento boton de descarga
            elemento_descarga = my_soup.find('a', class_='test-bookpdf-link')
            enlace_descarga = urlbase + elemento_descarga.get('href')
            
            # descargar y renombrar
            os.system("cd "+ dir_descargas + "/" + categoria +" && wget " + enlace_descarga + " -O " + nombre_articulo)
            time.sleep(5)
        except:
            print("no se puedo obtener el pdf")

read_text()